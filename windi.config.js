import { defineConfig } from "windicss/helpers";

export default defineConfig({
  extract: {
    include: ["**/*.{jsx,tsx,css}"],
    exclude: ["node_modules", ".git", ".next"],
  },
  theme: {
    container: {
      center: true,
      padding: {
        DEFAULT: "1rem",
        sm: "2rem",
        lg: "4rem",
        xl: "5rem",
        "2xl": "6rem",
      },
    },
    extend: {
      colors: {
        primary: {
          100: "#ead8da",
          200: "#d5b2b5",
          300: "#bf8b90",
          400: "#aa656b",
          500: "#953e46",
          600: "#773238",
          700: "#59252a",
          800: "#3c191c",
          900: "#1e0c0e",
        },
        secondary: {
          100: "#fdf9f5",
          200: "#fbf4ec",
          300: "#f8eee2",
          400: "#f6e9d9",
          500: "#f4e3cf",
          600: "#c3b6a6",
          700: "#92887c",
          800: "#625b53",
          900: "#312d29",
        },
      },
    },
  },
  plugins: [require("windicss/plugin/aspect-ratio")],
});
