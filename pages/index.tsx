import type { NextPage } from "next";
import Head from "next/head";
import Masonry from "react-masonry-css";
import { BsArrowUpShort, BsFillSlashCircleFill, BsPlus } from "react-icons/bs";
import Navbar from "../components/Navbar";
import Grids from "../components/Grids";
import Activity from "../components/Activity";
import Channel from "../components/Channel";
import MainMenu from "../components/MainMenu";
import Profile from "../components/Profile";

const Home: NextPage = () => {
  return (
    <div className="container">
      <Head>
        <title>Home - SocialNetwork</title>
        <meta name="description" content="Social Network" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Navbar />
      <Masonry
        breakpointCols={{ default: 2, 1024: 1 }}
        className="my-masonry-grid text-primary-500"
        columnClassName="my-masonry-grid_column"
      >
        <Grids
          type="Videos"
          text="Upload Your own Video"
          icon={<BsArrowUpShort size={24} />}
        />
        <Activity />
        <Grids
          type="Peoples"
          text="Show Your Work"
          icon={<BsFillSlashCircleFill size={16} />}
        />
        <Channel />
        <Grids
          type="Documents"
          text="Share Your Document"
          icon={<BsPlus size={24} />}
        />
      </Masonry>

      <MainMenu />
      <Profile />
    </div>
  );
};

export default Home;
