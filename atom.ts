import { atom } from "recoil";

export const isMenuState = atom({
  key: "isMenuState",
  default: false,
});

export const isProfileState = atom({
  key: "isProfileState",
  default: false,
});
