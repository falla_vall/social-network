import React from "react";
import { useRecoilState } from "recoil";
import Modal from "./Modal";
import { isMenuState } from "./../atom";

const MainMenu: React.FC = () => {
  const menus = [
    { name: "Videos" },
    { name: "People" },
    { name: "Documents" },
    { name: "Events" },
    { name: "Communities" },
    { name: "Favorites" },
    { name: "Channels" },
  ];
  return (
    <Modal visState={useRecoilState(isMenuState)}>
      <div className="flex justify-center items-center mx-4 p-4 border-b border-b-primary-200 text-xl font-semibold text-primary-500">
        Main Menu
      </div>
      <div className="flex flex-col py-4 text-primary-500">
        {menus.map((menu, index) => (
          <button
            key={index}
            className="p-4 text-lg hover:(bg-secondary-600) transition-colors duration-200"
          >
            {menu.name}
          </button>
        ))}
      </div>
    </Modal>
  );
};

export default MainMenu;
