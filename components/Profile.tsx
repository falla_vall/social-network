import React from "react";
import Image from "next/image";
import { useRecoilState } from "recoil";
import { HiUpload } from "react-icons/hi";
import { RiLogoutCircleRFill } from "react-icons/ri";
import { isProfileState } from "../atom";
import Modal from "./Modal";

const Profile: React.FC = () => {
  const menus = [
    { name: "My Profile" },
    { name: "Edit Profile" },
    { name: "Security" },
  ];
  return (
    <Modal visState={useRecoilState(isProfileState)}>
      <div className="relative flex flex-col justify-center items-center mx-4 p-4 border-b border-b-primary-200 text-xl font-semibold text-primary-500">
        <div className="-mt-12">
          <Image
            src="/profile.jpg"
            alt="profile"
            width={84}
            height={84}
            objectFit="cover"
            className="rounded-lg"
          />
        </div>
        <div className="text-xl font-semibold mt-4">Fallah Andy Prakasa</div>
        <span className="text-sm font-thin">Full-stack Developer</span>
        <button className="flex items-center justify-center gap-2 w-full bg-primary-500 text-secondary-500 my-3 p-2">
          <HiUpload size={20} />
          <span className="font-semibold">Start Upload</span>
        </button>
      </div>
      <div className="flex flex-col pt-4 pb-8 text-primary-500">
        {menus.map((menu, index) => (
          <button
            key={index}
            className="p-4 text-lg hover:(bg-secondary-600) transition-colors duration-200"
          >
            {menu.name}
          </button>
        ))}
      </div>
      <button className="flex items-center gap-1 w-full justify-center rounded-b-xl bg-secondary-600 text-primary-500 font-semibold p-4 border-t border-t-secondary-700">
        <span>Log Out</span>
        <RiLogoutCircleRFill size={16} />
      </button>
    </Modal>
  );
};

export default Profile;
