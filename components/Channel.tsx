import Image from "next/image";
import React from "react";
import { GoPlusSmall } from "react-icons/go";
import { BsArrowRightShort } from "react-icons/bs";

const Channel: React.FC = () => {
  const providers = [
    "Google",
    "Dribble",
    "Microsoft",
    "Behance",
    "Weather Channel",
    "Gurus",
    "iMedia",
    "Creativeye",
    "Khan Studios",
    "Yahoo",
  ];
  const isImageEnabled = process.env.NEXT_PUBLIC_ENABLE_IMAGE === "true";
  return (
    <>
      <div className="flex justify-between items-end text-secondary-500 py-4 border-b-3 border-secondary-500">
        <h1 className="text-lg md:text-xl lg:text-2xl xl:text-3xl font-semibold">
          Channels
        </h1>
        <a
          href="#"
          className="flex items-center text-sm md:text-md hover:(underline) text-right"
        >
          <span>Browse All Channels</span>
          <BsArrowRightShort size={16} />
        </a>
      </div>
      <div className="grid grid-cols-2 gap-x-2 gap-y-4 py-4 border-b-2 border-b-secondary-500">
        {providers.map((provider, index) => (
          <a
            key={index}
            href="#"
            className="relative flex items-end bg-secondary-500 aspect-video group"
          >
            {isImageEnabled && (
              <Image
                src="https://picsum.photos/800/450"
                alt="example"
                layout="fill"
              />
            )}
            <button className="hidden absolute top-0 right-0 bg-primary-600 w-5 h-5 group-hover:(block) hover:(bg-secondary-100)">
              <GoPlusSmall
                size={16}
                className="fill-secondary-500 absolute -top-1px -right-1px"
              />
            </button>
            <div className="absolute top-2 right-0.5 bg-secondary-500 w-7 h-4 rotate-45 transform"></div>
            <div
              className={`${
                isImageEnabled ? "text-secondary-500" : "text-primary-500"
              } text-xl font-semibold p-2 z-1`}
            >
              {provider}
            </div>
          </a>
        ))}
      </div>
    </>
  );
};

export default Channel;
