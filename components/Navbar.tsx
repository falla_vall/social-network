import React from "react";
import Link from "next/link";
import { useRecoilState } from "recoil";
import cn from "classnames";
import {
  HiMenu,
  HiUpload,
  HiUser,
  HiSearch,
  HiArrowSmUp,
} from "react-icons/hi";
import { isMenuState, isProfileState } from "../atom";

const Navbar: React.FC = () => {
  const styles = {
    navbar: "flex flex-row justify-between items-center py-4",
    hover:
      "hover:(text-secondary-100) group-hover:(text-secondary-100) transition-colors duration-200",
    hoverBG:
      "hover:(bg-secondary-100) group-hover:(bg-secondary-100) transition-colors duration-200",
    start: "flex items-center md:hidden basis-1/3 sm:basis-auto pr-2",
    title:
      "text-lg sm:text-2xl md:text-4xl lg:text-6xl font-thin basis-1/3 sm:basis-auto text-center",
    end: "flex flex-row items-center sm:items-stretch justify-end gap-4 basis-1/3 sm:basis-full",
    search:
      "hidden sm:flex justify-between items-center bg-secondary-500 rounded-sm",
    searchMobile:
      "flex sm:hidden justify-between items-center bg-secondary-500 rounded-sm",
    searchIcon: "text-primary-500 mr-2",
    input:
      "bg-transparent outline-none sm:w-24 md:w-36 lg:w-52 xl:w-96 p-2 text-primary-500 placeholder-primary-400",
    hidden: "sm:hidden",
    upload:
      "hidden sm:flex justify-center items-center gap-2px bg-secondary-500 rounded-sm text-primary-500 px-3",
    profile: "hidden sm:flex flex-row items-stretch relative gap-1 group",
    avatar:
      "flex items-center bg-secondary-500 text-primary-500 rounded-sm px-3",
    fullname: "flex flex-col items-start justify-center",
    lastname: "text-xs",
    dot: "flex justify-center items-center absolute -top-2 -right-4 w-4 h-4 bg-secondary-500 rounded-full text-10px text-primary-500",
    menus:
      "hidden md:flex flex-row gap-2 lg:gap-4 py-2 border-t border-b border-secondary-500",
    menu: "flex flex-row gap-2 lg:gap-4",
  };
  const menus = [
    { name: "Videos" },
    { name: "People" },
    { name: "Documents" },
    { name: "Events" },
    { name: "Communities" },
    { name: "Favorites" },
    { name: "Channels" },
  ];

  const [_isMenu, setIsMenu] = useRecoilState(isMenuState);
  const [_isProfile, setIsProfile] = useRecoilState(isProfileState);
  return (
    <>
      <div className={styles.navbar}>
        <button
          onClick={() => setIsMenu(true)}
          className={cn(styles.start, styles.hover)}
        >
          <HiMenu size={32} />
        </button>
        <Link href="/">
          <a className={cn(styles.title, styles.hover)}>
            <span className="font-bold">Social</span>Network
          </a>
        </Link>
        <div className={styles.end}>
          <button className={cn(styles.hover, styles.hidden)}>
            <HiUpload size={32} />
          </button>
          <button
            onClick={() => setIsProfile(true)}
            className={cn(styles.hover, styles.hidden)}
          >
            <HiUser size={32} />
          </button>
          <div className={cn(styles.search, styles.hoverBG)}>
            <input
              type="text"
              name="search"
              id="search"
              placeholder="Find..."
              className={styles.input}
            />
            <HiSearch size={18} className={styles.searchIcon} />
          </div>
          <button className={cn(styles.upload, styles.hoverBG)}>
            <HiArrowSmUp size={18} />
            <span>Upload</span>
          </button>
          <button className={styles.profile}>
            <div className={cn(styles.avatar, styles.hoverBG)}>
              <HiUser size={16} />
            </div>
            <div className={cn(styles.fullname, styles.hover)}>
              <div>Waseed</div>
              <span className={styles.lastname}>Arshad</span>
            </div>
            <div className={cn(styles.dot, styles.hoverBG)}>8</div>
          </button>
        </div>
      </div>
      <div className={cn(styles.searchMobile, styles.hoverBG)}>
        <input
          type="text"
          name="search"
          id="search"
          placeholder="Find..."
          className={styles.input}
        />
        <HiSearch size={18} className={styles.searchIcon} />
      </div>
      <div className={styles.menus}>
        {menus.map((menu, index) => (
          <button key={index} className={cn(styles.menu, styles.hover)}>
            <div className="text-lg">{menu.name}</div>
            <span>/</span>
          </button>
        ))}
      </div>
    </>
  );
};

export default Navbar;
