import React from "react";
import Image from "next/image";
import { useMediaQuery } from "react-responsive";
import { IoIosClose } from "react-icons/io";
import { TbMessageCircle } from "react-icons/tb";
import { BsArrowRightShort } from "react-icons/bs";

const Activity: React.FC = () => {
  const isMd = useMediaQuery({ minWidth: 768 });
  const isImageEnabled = process.env.NEXT_PUBLIC_ENABLE_IMAGE === "true";
  return (
    <>
      <div className="flex justify-between items-end text-secondary-500 pb-4 border-b-3 border-secondary-500">
        <h1 className="text-lg md:text-xl lg:text-2xl xl:text-3xl font-semibold">
          Activity
        </h1>
        <a
          href="#"
          className="flex items-center text-sm md:text-md hover:(underline) text-right"
        >
          <span>View Timeline{isMd && " / Filter activities"}</span>
          <BsArrowRightShort size={16} />
        </a>
      </div>
      <div className="flex flex-col gap-4 lg:gap-2 py-4 border-b-2 border-b-secondary-500">
        {Array.from({ length: 6 }, (_, i) => (
          <a
            key={i}
            href="#"
            className="flex flex-row items-center gap-4 bg-primary-400 lg:bg-inherit p-1 relative w-full border-3 border-transparent group hover:(border-secondary-500)"
          >
            <button className="hidden absolute -top-1px -right-1px bg-secondary-500 w-4 h-4 group-hover:(block) hover:(bg-secondary-100)">
              <IoIosClose
                size={12}
                className="fill-primary-500 absolute -top-1px -right-1px"
              />
            </button>
            <div className="hidden absolute group-hover:(block) top-5.5px right-0.7px bg-primary-400 lg:bg-primary-500 w-6 h-4 rotate-45 transform"></div>
            <div className="bg-secondary-500 relative h-24 aspect-square sm:aspect-video">
              {isImageEnabled && (
                <Image
                  src="https://picsum.photos/800/450"
                  alt="example"
                  layout="fill"
                />
              )}
            </div>
            <div className="flex flex-col justify-center xl:gap-2 2xl:gap-3 text-secondary-500">
              <div className="text-xl font-semibold">
                John Stainior{" "}
                <span className="text-sm font-thin">commented</span>
              </div>
              <span className="text-sm font-thin text-ellipsis">
                Well, I am liking it how it captures the audio...
              </span>
              <span className="flex gap-1 text-xs font-thin">
                <TbMessageCircle size={16} className="fill-secondary-500" />
                <span className="text-secondary-500 text-xs">
                  2 minutes ago
                </span>
              </span>
            </div>
          </a>
        ))}
      </div>
    </>
  );
};

export default Activity;
