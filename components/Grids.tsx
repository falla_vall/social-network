import React from "react";
import Image from "next/image";
import { useMediaQuery } from "react-responsive";
import { BsArrowRightShort } from "react-icons/bs";

type Props = {
  type: string;
  text: string;
  icon: JSX.Element;
};

const Grids: React.FC<Props> = ({ type, text, icon }) => {
  const isImageEnabled = process.env.NEXT_PUBLIC_ENABLE_IMAGE === "true";
  const splitted = text.split(" ");
  const isMd = useMediaQuery({ minWidth: 768 });
  return (
    <>
      <div className="w-full flex items-end">
        <div className="basis-full md:basis-2/3 flex justify-between items-end text-secondary-500">
          <h1 className="text-lg md:text-xl lg:text-2xl xl:text-3xl font-semibold">
            {type}
          </h1>
          <a
            href="#"
            className="flex items-center text-sm md:text-md hover:(underline)"
          >
            <span>Browse All {type}</span>
            <BsArrowRightShort size={16} />
          </a>
        </div>
      </div>
      <div className="flex no-scrollbar <md:aspect-video overflow-x-auto md:(grid grid-cols-3 overflow-visible) gap-4 text-secondary-500 pb-4">
        {Array.from({ length: 6 }, (_, i) => (
          <button
            key={i}
            className={`${
              type === "Videos"
                ? isMd
                  ? "aspect-photo"
                  : "aspect-video"
                : "aspect-photo"
            } flex justify-center items-center relative p-2 md:( first:(col-span-2 row-span-2 first:text-3xl)) bg-secondary-500 group last:(bg-inherit border border-secondary-500) hover:(scale-105 shadow-2xl transform transition-transform duration-200 ease-in-out)`}
          >
            {i < 5 ? (
              <>
                {isImageEnabled && (
                  <Image
                    src="https://picsum.photos/400/300"
                    alt="example"
                    layout="fill"
                  />
                )}
                <div
                  className={`${
                    isImageEnabled ? "text-secondary-500" : "text-primary-500"
                  } absolute bottom-0 flex justify-between items-end w-full pl-2 pr-3 py-1 stroke-2 stroke-secondary-500 z-10`}
                >
                  <div className="flex flex-col items-start text-left">
                    <div
                      className={`${
                        i > 0 ? "md:hidden" : "block"
                      } font-semibold`}
                    >
                      How to Improve your skills
                    </div>
                    <div
                      className={`${
                        i > 0 ? "text-lg" : "text-xl"
                      } font-semibold`}
                    >
                      Waseem Arshad
                    </div>
                  </div>
                  <span
                    className={`${i > 0 ? "text-xs" : "text-sm"} text-right`}
                  >
                    7129402 views
                  </span>
                </div>
              </>
            ) : (
              <div className="flex flex-row items-center gap-4">
                <div className="flex justify-center items-center w-12 h-12 rounded-full border-3 border-secondary-500">
                  {icon}
                </div>
                <span className="text-xl whitespace-pre-wrap text-left">
                  {splitted[0]}
                  <br />
                  {splitted.slice(1).join(" ")}
                </span>
              </div>
            )}
          </button>
        ))}
      </div>
    </>
  );
};

export default Grids;
